export default () => {
  const fr = Array.from(document.querySelectorAll('.form-rent-elements'));
  fr.forEach((block) => {
    const rows = block.querySelector('.form-rent-elements__rows');
    const example = block.querySelectorAll('.form-rent-elements__row')[0];
    const addRow = block.querySelector('.form-rent-elements__add');

    const del = (row) => {
      const delRow = row.querySelector('.form-rent-elements__remove a');
      delRow.addEventListener('click', (e) => {
        e.preventDefault();
        if (Array.from(e.target.parentElement.parentElement.children).length !== 1) {
          e.target.parentElement.parentElement.remove();
        }
      });
    };

    let rowsCount = 1;

    del(example);

    addRow.addEventListener('click', (e) => {
      e.preventDefault();
      const newRow = example.cloneNode(true);
      const inpSel = [
        ...Array.from(newRow.querySelectorAll('.form-input__field')),
        ...Array.from(newRow.querySelectorAll('.form-input__select')),
      ];
      inpSel.forEach((input) => {
        const i = input;
        const label = i.previousElementSibling;
        i.id = `${i.id.slice(0, -1)}${rowsCount}]`;
        i.value = '';
        if (i.options) { i.options[0].selected = true; }
        label.htmlFor = i.id;
      });
      del(newRow);
      rows.append(newRow);
      rowsCount += 1;
    });
  });
};
