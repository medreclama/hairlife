export default function bannerVolosy24() {
  const bv24slides = Array.from(document.querySelectorAll('.banner-volosy24__slide'));
  if (bv24slides.length > 0) {
    const slidesNumber = bv24slides.length - 1;
    let i = 0;
    bv24slides[0].classList.add('banner-volosy24__slide--active');
    setInterval(() => {
      if (i < slidesNumber) {
        bv24slides[i].classList.remove('banner-volosy24__slide--active');
        i += 1;
        bv24slides[i].classList.add('banner-volosy24__slide--active');
      } else {
        bv24slides[i].classList.remove('banner-volosy24__slide--active');
        i = 0;
        bv24slides[i].classList.add('banner-volosy24__slide--active');
      }
    }, 5000);
  }
}
