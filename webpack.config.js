const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/scripts/script.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
    ],
  },
};
